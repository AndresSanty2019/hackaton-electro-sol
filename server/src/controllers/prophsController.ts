import { Request, Response} from 'express';
import pool from '../database';

class ProphsController {
   public async list (req: Request, res: Response) {
     const prophs = await pool.query('SELECT * FROM prophs');
     res.json(prophs);
   }

   public async getOne(req: Request, res: Response): Promise<any> {
      const { id } = req.params;
      const prophs = await pool.query('SELECT * FROM prophs WHERE id_prophs = ?', [id]);
      console.log(prophs.length);
      if (prophs.length > 0) {
          return res.json(prophs[0]);
      }
      res.status(404).json({ text: "estos datos no existen" });
  }
   public async create (req: Request, res: Response): Promise<void>{
    await pool.query('INSERT INTO prophs set ?', [req.body]);
    res.json({message: 'Los datos de guardaron correctamente'});
   }
   public async update (req: Request, res: Response) {
      const { id } = req.params;
      await pool.query('UPDATE prophs set ? WHERE id_prophs = ?', [req.body, id]);
      res.json({message: 'Los datos se actualizaron '});
     }
   public async delete (req: Request, res: Response) {
      const { id } = req.params;
      await pool.query('DELETE FROM prophs WHERE id_prophs = ?', [id]);
      res.json({message: 'los datos se eliminaron '});
     }
     
}

 const prophsController = new ProphsController(); 
 export default prophsController;