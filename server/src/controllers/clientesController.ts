import { Request, Response} from 'express';
import pool from '../database';

class ClientesController {
   public async list (req: Request, res: Response) {
     const clientes = await pool.query('SELECT * FROM clientes');
     res.json(clientes);
   }

   public async getOne(req: Request, res: Response): Promise<any> {
      const { id } = req.params;
      const clientes = await pool.query('SELECT * FROM clientes WHERE id_cliente = ?', [id]);
      console.log(clientes.length);
      if (clientes.length > 0) {
          return res.json(clientes[0]);
      }
      res.status(404).json({ text: "Este cliente no esta dado de alta" });
  }
   public async create (req: Request, res: Response): Promise<void>{
    await pool.query('INSERT INTO clientes set ?', [req.body]);
    res.json({message: 'El Cliente se dio de Alta Exelentemente'});
   }
   public async update (req: Request, res: Response) {
      const { id } = req.params;
      await pool.query('UPDATE clientes set ? WHERE id_cliente = ?', [req.body, id]);
      res.json({message: 'El Cliente se Actualizo Exelentemente '});
     }
   public async delete (req: Request, res: Response) {
      const { id } = req.params;
      await pool.query('DELETE FROM clientes WHERE id_cliente = ?', [id]);
      res.json({message: 'El Cliente se Elimino Exelentemente '});
     }
     
}

 const clientesController = new ClientesController(); 
 export default clientesController;