import { Request, Response} from 'express';
import pool from '../database';

class ServiciosController {
   public async list (req: Request, res: Response) {
     const servicios = await pool.query('SELECT * FROM servicios');
     res.json(servicios);
   }

   public async getOne(req: Request, res: Response): Promise<any> {
      const { id } = req.params;
      const servicios = await pool.query('SELECT * FROM servicios WHERE id_servicios = ?', [id]);
      console.log(servicios.length);
      if (servicios.length > 0) {
          return res.json(servicios[0]);
      }
      res.status(404).json({ text: "estos datos no existen" });
  }
   public async create (req: Request, res: Response): Promise<void>{
    await pool.query('INSERT INTO servicios set ?', [req.body]);
    res.json({message: 'Los datos de guardaron correctamente'});
   }
   public async update (req: Request, res: Response) {
      const { id } = req.params;
      await pool.query('UPDATE servicios set ? WHERE id_servicios = ?', [req.body, id]);
      res.json({message: 'Los datos se actualizaron '});
     }
   public async delete (req: Request, res: Response) {
      const { id } = req.params;
      await pool.query('DELETE FROM servicios WHERE id_servicios = ?', [id]);
      res.json({message: 'los datos se eliminaron '});
     }
     
}

 const serviciosController = new ServiciosController(); 
 export default serviciosController;