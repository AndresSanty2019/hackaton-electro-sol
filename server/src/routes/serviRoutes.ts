import { Router } from 'express';
import serviController from '../controllers/serviController';

class ServiRoutes {
    public router: Router = Router();

    constructor(){
        this.config();
    }

    config(): void {
        this.router.get('/',serviController.list);
        this.router.get('/:id',serviController.getOne);
        this.router.post('/', serviController.create);
        this.router.put('/:id', serviController.update);
        this.router.delete('/:id', serviController.delete );

    }
}

const serviRoutes = new ServiRoutes();
export default serviRoutes.router;