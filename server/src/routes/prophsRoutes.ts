import { Router } from 'express';
import prophsController from '../controllers/prophsController';

class ProphsRoutes {
    public router: Router = Router();

    constructor(){
        this.config();
    }

    config(): void {
        this.router.get('/',prophsController.list);
        this.router.get('/:id',prophsController.getOne);
        this.router.post('/', prophsController.create);
        this.router.put('/:id', prophsController.update);
        this.router.delete('/:id', prophsController.delete );

    }
}

const prophsRoutes = new ProphsRoutes();
export default prophsRoutes.router;