"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database"));
class ProphsController {
    list(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const prophs = yield database_1.default.query('SELECT * FROM prophs');
            res.json(prophs);
        });
    }
    getOne(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const prophs = yield database_1.default.query('SELECT * FROM prophs WHERE id_prophs = ?', [id]);
            console.log(prophs.length);
            if (prophs.length > 0) {
                return res.json(prophs[0]);
            }
            res.status(404).json({ text: "estos datos no existen" });
        });
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield database_1.default.query('INSERT INTO prophs set ?', [req.body]);
            res.json({ message: 'Los datos de guardaron correctamente' });
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            yield database_1.default.query('UPDATE prophs set ? WHERE id_prophs = ?', [req.body, id]);
            res.json({ message: 'Los datos se actualizaron ' });
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            yield database_1.default.query('DELETE FROM prophs WHERE id_prophs = ?', [id]);
            res.json({ message: 'los datos se eliminaron ' });
        });
    }
}
const prophsController = new ProphsController();
exports.default = prophsController;
