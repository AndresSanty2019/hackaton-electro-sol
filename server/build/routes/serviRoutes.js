"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const serviController_1 = __importDefault(require("../controllers/serviController"));
class ServiRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/', serviController_1.default.list);
        this.router.get('/:id', serviController_1.default.getOne);
        this.router.post('/', serviController_1.default.create);
        this.router.put('/:id', serviController_1.default.update);
        this.router.delete('/:id', serviController_1.default.delete);
    }
}
const serviRoutes = new ServiRoutes();
exports.default = serviRoutes.router;
