"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const prophsController_1 = __importDefault(require("../controllers/prophsController"));
class ProphsRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/', prophsController_1.default.list);
        this.router.get('/:id', prophsController_1.default.getOne);
        this.router.post('/', prophsController_1.default.create);
        this.router.put('/:id', prophsController_1.default.update);
        this.router.delete('/:id', prophsController_1.default.delete);
    }
}
const prophsRoutes = new ProphsRoutes();
exports.default = prophsRoutes.router;
